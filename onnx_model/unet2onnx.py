import tensorflow as tf
import tensorflow.layers as layers
import tf2onnx
import tf2onnx.convert as convert
import os
os.environ["CUDA_VISIBLE_DEVICES"]="2"



def UNET(input):
    conv1 = layers.conv2d(input,64,3,activation=tf.nn.relu,padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    conv1 = layers.conv2d(conv1, 64, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    pool1 = layers.max_pooling2d(conv1,[2,2],[2,2])
    conv2 = layers.conv2d(pool1, 128, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    conv2 = layers.conv2d(conv2, 128, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    pool2 = layers.max_pooling2d(conv2, [2, 2], [2, 2])
    conv3 = layers.conv2d(pool2, 256, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    conv3 = layers.conv2d(conv3, 256, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    pool3 = layers.max_pooling2d(conv3, [2, 2], [2, 2])
    conv4 = layers.conv2d(pool3, 512, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    conv4 = layers.conv2d(conv4, 512, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    pool4 = layers.max_pooling2d(conv4, [2, 2], [2, 2])

    conv5 = layers.conv2d(pool4, 1024, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    conv5 = layers.conv2d(conv5, 1024, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    up6 = layers.conv2d_transpose(conv5,512,2,strides=(2,2),padding='SAME')

    merge6 = tf.concat([conv4, up6], axis=3)

    conv6 = layers.conv2d(merge6, 512, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    conv6 = layers.conv2d(conv6, 512, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    up7 = layers.conv2d_transpose(conv6, 256, 2, strides=(2, 2), padding='SAME')

    merge7 = tf.concat([conv3, up7], axis=3)

    conv7 = layers.conv2d(merge7, 256, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    conv7 = layers.conv2d(conv7, 256, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)

    up8 = layers.conv2d_transpose(conv7, 128, 2, strides=(2, 2), padding='SAME')

    merge8 = tf.concat([conv2, up8], axis=3)

    conv8 = layers.conv2d(merge8, 128, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    conv8 = layers.conv2d(conv8, 128, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    up9 = layers.conv2d_transpose(conv8, 64, 2, strides=(2, 2), padding='SAME')

    merge9 = tf.concat([conv1, up9], axis=3)
    conv9 = layers.conv2d(merge9, 64, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    conv9 = layers.conv2d(conv9, 64, 3, activation=tf.nn.relu, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    conv9 = layers.conv2d(conv9, 2, 3, activation=tf.nn.softmax, padding='SAME',
                          kernel_initializer=tf.initializers.truncated_normal)
    #conv10 = layers.conv2d(conv9, 1, 1, activation=tf.nn.softmax, padding='SAME',
     #                     kernel_initializer=tf.initializers.truncated_normal)
    return conv9

with tf.Session() as sess:
    x = tf.placeholder(tf.float32, [4,512, 512,1], name="input")
    y = UNET(x)
    sess.run(tf.global_variables_initializer())
    OUTPUT_NODE_NAME = y.op.name
    #convert.main()
    output_graph_def = tf.graph_util.convert_variables_to_constants(sess,tf.get_default_graph().as_graph_def(),list([OUTPUT_NODE_NAME]))
    with tf.gfile.GFile('maxQ_frozen.pb', "wb") as f:
        f.write(output_graph_def.SerializeToString())
    onnx_graph = tf2onnx.tfonnx.process_tf_graph(sess.graph, input_names=["input:0"], output_names=OUTPUT_NODE_NAME)
    model_proto = onnx_graph.make_model("test")
    with open("/maxQModel.onnx", "wb") as f:
        f.write(model_proto.SerializeToString())



