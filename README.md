# ALOHA MAXQ medical use case data and details

This repository contains information of the medical use case.

This is shared for research purposes only and is not to be copied and used outside Aloha2020 group.

The 'toy_dataset' folder contains a toy benchmark for the Unet train.
 
The 'ref_model' folder contains a model trained using TensorFlow on the toy dataset, with the parameters below.

```
* Split on train/validation: 0.8 
* Learning Rate: 0.001
* Batch Size: 5
* Number of Learning epochs: 50
* Loss function: dice
* Optimizer function: adam
* Nonlinearity: relu
* Image width: 320
* Image height: 320
* Number of filters on first layer: 64

```

The loss convergance is plotted in grey:

![toy_dataset](dice.png)

The 'onnx_model' folder contains the code for converting the tf model to oonx.

Toolflow medical use case: we train the unet model on brain CT data in order to detect bleeds. The medical domain specific requiremnets are fast processing, high accuracy, privacy of patients.

